package pucrs.alpro2;

import java.util.Iterator;

public class App {

	public static void main(String[] args) {
		ListDoubleLinked<Integer> lista = new ListDoubleLinked<>();
		System.out.println("Teste de ListDoubleLinked");
		System.out.println("Tam: "+lista.size());
		lista.add(1);
		lista.add(2);
		lista.add(4);
		lista.add(5);
		lista.add(6);
		System.out.println("Tam: "+lista.size());
		System.out.println(lista);
		lista.add(2, 3); // insere o valor 3 na pos. 2
		System.out.println(lista);
		lista.add(0, 0); // insere o valor 0 no início
		System.out.println(lista);
		lista.add(7);
		lista.add(8);
		lista.add(90); // teste de conflito
		System.out.println(lista);
		
		System.out.println("Removido: "+lista.remove(8));  // apaga o 8
//		System.out.println("Removido: "+lista.remove(90));  // tenta apagar o 90, mas NÃO funciona!
		System.out.println("Removido (90): "+lista.remove(new Integer(90)));  // apaga o 90 usando o segundo remove
		System.out.println("Removido: "+lista.remove(0));  // apaga o primeiro elemento

		System.out.println();
		
		System.out.println("Percorrendo com for...each:");
		// Percorrendo a lista com for...each (iterador)
		for(int valor: lista)
			System.out.println(valor);
		
		// Percorrendo a lista do fim para o início com o iterador reverso
		System.out.println("Percorrendo com iterador reverso:");
		Iterator<Integer> it = lista.reverseIterator();
		while(it.hasNext())
			System.out.println(it.next());
		
		System.out.println("Tam: "+lista.size());
		System.out.println(lista);
	}
}
