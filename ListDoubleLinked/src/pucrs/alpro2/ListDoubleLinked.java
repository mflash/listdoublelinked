package pucrs.alpro2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListDoubleLinked<E> implements ListTAD<E>, Iterable<E> {

	private class Node<E> {
		public E element;
		public Node<E> next;
		public Node<E> prev;

		public Node(E e) {
			element = e;
			prev = next = null;
		}
	}

	private Node<E> header;
	private Node<E> trailer;
	private int count;

	// Construtor
	public ListDoubleLinked() {
		clear();
	}

	@Override
	public void add(E e) {
		Node<E> n = new Node<>(e);
		Node<E> last = trailer.prev;
		n.prev = last;
		n.next = trailer;
		last.next = n;
		trailer.prev = n;
		count++;
	}

	@Override
	public void add(int index, E element) {
		if (index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		Node<E> n = new Node<>(element);

		// Pára no elemento desejado
		Node<E> target = header.next;
		for (int pos = 0; pos < index; pos++)
			target = target.next;

		Node<E> ant = target.prev;

		n.next = target;
		n.prev = ant;
		ant.next = n;
		target.prev = n;
		count++;
	}

	@Override
	public E get(int index) {
		if ((index < 0) || (index >= count)) {
			throw new IndexOutOfBoundsException("Pos. inválida: " + index);
		}
		Node<E> target = header.next;
		for (int i = 0; i < index; i++) {
			target = target.next;
		}
		return target.element;
	}

	@Override
	public int indexOf(E e) {
		Node<E> aux = header.next;
		int pos = 0;

		while (aux != trailer) {
			if (aux.element.equals(e))
				return pos;
			aux = aux.next;
		}

		return -1;
	}

	@Override
	public void set(int index, E element) {
		if ((index < 0) || (index >= count)) {
			throw new IndexOutOfBoundsException("Pos. inválida: " + index);
		}
		Node<E> target = header.next;
		for (int i = 0; i < index; i++) {
			target = target.next;
		}
		target.element = element;

	}

	@Override
	public boolean remove(E e) {
		Node<E> target = header.next;
		while (target != trailer) {
			if (target.element.equals(e)) {
				target.prev.next = target.next;
				target.next.prev = target.prev;
				count--;
				return true;
			}
			target = target.next;
		}
		return false;
	}

	@Override
	public E remove(int index) {
		if ((index < 0) || (index >= count)) {
			throw new IndexOutOfBoundsException("Pos. inválida: " + index);
		}
		Node<E> target = header.next;
		for (int i = 0; i < index; i++) {
			target = target.next;
		}
		target.prev.next = target.next;
		target.next.prev = target.prev;
		count--;
		return target.element;
	}

	@Override
	public boolean isEmpty() {
		return count == 0;
	}

	@Override
	public int size() {
		return count;
	}

	@Override
	public boolean contains(E e) {
		return indexOf(e) != -1;
	}

	@Override
	public void clear() {
		header = new Node<E>(null);
		trailer = new Node<E>(null);
		header.next = trailer;
		trailer.prev = header;
		count = 0;
	}

	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		Node<E> atual = header.next;
		aux.append("[ ");
		while (atual != trailer) {
			aux.append(atual.element + " ");
			atual = atual.next;
		}
		aux.append("]");
		return aux.toString();
	}

	@Override
	public Iterator iterator() {
		return new Iterator<E>() {

			private Node<E> atual = header.next;

			@Override
			public boolean hasNext() {
				return atual != trailer;
			}

			@Override
			public E next() {
				if (atual == trailer)
					throw new NoSuchElementException();
				E item = atual.element;
				atual = atual.next;
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}
	
	public Iterator reverseIterator() {
		return new Iterator<E>() {

			private Node<E> atual = trailer.prev;

			@Override
			public boolean hasNext() {
				return atual != header;
			}

			@Override
			public E next() {
				if (atual == header)
					throw new NoSuchElementException();
				E item = atual.element;
				atual = atual.prev;
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}
}
